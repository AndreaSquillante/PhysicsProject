using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pig : MonoBehaviour
{
   

    void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > 1f)
        {
            Destroy();
        }
    }

    private void Destroy()
    {
      
        GameManager.Instance.AddScore(5000, transform.position, Color.green);
        Destroy(gameObject);
    }
}
