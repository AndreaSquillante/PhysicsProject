﻿using UnityEngine;

public class Ice : MonoBehaviour
{
    
    void OnCollisionEnter(Collision collision)
    {
        if (collision.relativeVelocity.magnitude > 5f)
        {
            Destroy();
        }
    }

    private void Destroy()
    {
     
        GameManager.Instance.AddScore(500, transform.position, Color.white);
        
        Destroy(gameObject);
    }
}
